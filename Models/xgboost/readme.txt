This xgboost code is meant to be a baseline or starting place for any xgboost model. 

These datafiles were created based on the kaggle dataset -  house-prices-advanced-regression-techniques, 
but the code was designed to be applied to any train/test dataset. 

Three scripts:

1) xgboost_model_building.py: contains functions needed to run the xgboost model, 
including preprocessing and model evaluation. Called by run_xgboost.py. 

General description of functions include:
plot_timeseries: plot quick timeseries
recode_to_binary: recode categorical variables to binary
perform_gridsearch_xg_regression: use of gridsearch for hypertuning parameters
xgboost_best_params: runs xgboost model using best_params output from gridsearch. The optimal set of parameters.
second_deviation_zero_search: identifies the correct number of features using the second derivative on an elbow shaped plot where y is r2 and x is number of features.

2) run_xgboost.py: Runs all functions required to produce xgboost result for kaggle competition relating to housing market. This file is specific to the dataset under investigation.  Link to data on
kaggle provided in file.

3) multiple_regression_model_building.py: file of functions originally for multiple regression model but some functions are also used in xgboost. This file needs to be loaded to run xgboost. File located in multiple_regression folder.

This code produces an r2 on test data of .88.  First submission to kaggle can be found here https://www.kaggle.com/tracyromano/competitions. 


Model performance:
r2= .88
kaggle ranking: 1522/5337
root mean squared logarithmic error: .12

