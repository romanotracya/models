# -*- coding: utf-8 -*-
"""
Created on Tue Nov 23 10:29:21 2021

@author: roman
multiple_regression_model_building.py in the models folder on gitlab must be present in directory. 
Current: https://www.kaggle.com/tracyromano/competitions 1502 of 5362

"""


#Import libraries
import os
import pandas as pd
import numpy as np
from xgboost import XGBRegressor
from sklearn.metrics import classification_report
#from sklearn.metrics import confusioni_matrix
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
import matplotlib.pyplot as plt
import seaborn as sn
from xgboost import plot_importance
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import mean_squared_error
import time 
os.chdir('')#add directory


exec(open('multiple_regression_model_building.py').read())
exec(open('xgboost_model_building.py').read())

train=pd.read_csv('train.csv')
test=pd.read_csv('test.csv')

prepro_test=recode_to_binary(test, code_all=test.shape()[1])
prepro=recode_to_binary(train,code_all=test.shape()[1])
predictor_name='SalePrice'
plot_timeseries(train, predictor_name)

#make sure train/test columns match. Once have column names post-recode
prepro=pd.concat([prepro[prepro_test.columns],prepro['SalePrice']],axis=1)
#change all objects to binary features (one hot encoding) and all int8 to int 

#Split the data into train and test
x_train, x_test, y_train, y_test=train_test_split(prepro.drop(['SalePrice'],axis=1),prepro['SalePrice'],random_state=0, test_size=.3)    
x_train=x_train.reset_index(drop=True)
x_test=x_test.reset_index(drop=True)
y_train=y_train.reset_index(drop=True)
y_test=y_test.reset_index(drop=True)


#identify optimal parameters using gridsearch
best_params, df= perform_gridsearch_xg_regression(data=pd.concat([x_train, y_train],axis=1),predictor_name='SalePrice', objective='reg:squarederror', estimator_type='regression', eval_metric_type='mae')
#model without feature selection,only optimized by grid search 
rmse, mae, score, Error_GS, model,y_prediction,residuals=xgboostR_best_params(best_params, x_train, y_train,x_test, y_test)
plot_residuals(y_prediction, residuals, y_test) # xgboost residuals can show multicollinearity- plot ok


#feature selection and create new dataset 
x,y=wrangle_for_select(x_train, y_train,predictor_name=predictor_name)
feature_names_list,scores,feature_answer, feature_list=foward_backward_selection(x,y,k_change=.01,k_max=75,is_step=1)
sd_plot,zero_search, k_chosen=second_deviation_zero_search(data=scores)

x_train_pro_fs,x_test_pro_fs=create_dataset_from_fs(feature_names_list,x_train_pro=pd.concat([x_train, y_train],axis=1),x_test_pro=pd.concat([x_test, y_test],axis=1),index=k_chosen,predictor_name='SalePrice')
        
x_test_pro_fs=x_test_pro_fs[x_train_pro_fs.columns]

#rerun model post-feature selection post- grid search
rmse, mae, score, Error_FS_GS, model,y_prediction,residuals=xgboost_best_params(best_params, x_train_pro_fs, y_train,x_test_pro_fs,y_test)
plot_residuals(y_prediction, residuals, y_test) # xgboost residuals can show multicollinearity- plot ok- view only


f = open("ModelPerformance" +  str(int(time.time())) +  ".txt","w")
f.write(str(Error_FS_GS))
f.close()
    
    
#examine test data
x_test_pro_fs=prepro_test[x_train_pro_fs.columns]
y_prediction_test = model.predict(x_test_pro_fs)
forkaggle=pd.concat([test['Id'],pd.DataFrame(y_prediction_test)],axis=1)
forkaggle.columns=(['Id','SalePrice'])


plot_importance(model)    