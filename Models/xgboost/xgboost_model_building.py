# -*- coding: utf-8 -*-
"""
Created on Fri Nov 19 10:50:57 2021

@author: roman
"""

#Import libraries
import os
import pandas as pd
import numpy as np
from xgboost import XGBRegressor
from sklearn.metrics import classification_report
#from sklearn.metrics import confusioni_matrix
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
import matplotlib.pyplot as plt
import seaborn as sn
from xgboost import plot_importance
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_squared_log_error
from xgboost import XGBClassifier
#os.chdir('') #add directory


def plot_timeseries(data, predictor_name):
    #this function will plot a timeseries where x is counts and y is value
    #data: any data file where a feature is to be examined in line plot
    #predictor_name: column_name of predictor or some other column name of interest
    plt.figure(figsize=(10, 7))
    plt.plot(data[predictor_name] )
    # Set the title and axis labels and plot grid
    plt.title(predictor_name)
    plt.grid()
    plt.show()
    
def recode_to_binary(data,predictor_name, code_all,drop):
    #xgboost does not process object nor category features and
    #these features must be convertedn some way to number. This def will
    #use a pandas get_dummies and recode each type within a feature into its own 
    #column as 1 or 0 (present or not). Basic recoding approach for creating baseline
    #change all uint8 to integers. Only category or objects with 10 or less levels
    #Method: identify all objects, drop them from dataset, and convert them to 1/0 features. then add them to 
    #the training dataset
    #model
    #input:
        #data: data that contains features to be recoded
        #code_all: number of levels per feature recoded. Suggestion is 10.
        #drop: if would like to drop variables that are getting recoded then 1
    #Output
        #preprocessed_data
    get_num_list=data.apply(lambda x: len(x.unique())).reset_index()
    get_num_list=get_num_list[(get_num_list[0]>2) & (get_num_list[0]<10)]['index']
    dtypes_list=data.drop(predictor_name,axis=1, errors='ignore').dtypes.reset_index()
    Cat_feat=pd.DataFrame(dtypes_list[(dtypes_list[0]=='object')  | (dtypes_list[0]=='category')]['index']).reset_index(drop=True)
    levels=data[Cat_feat['index']].apply(lambda x: len(x.unique())).reset_index()
    levels.columns=(['index','groups'])
    levels=(levels[levels['groups']<=code_all]).reset_index(drop=True)
    levels=pd.DataFrame(pd.concat([Cat_feat['index'], get_num_list]).unique())
    levels.columns=(['index'])
    make_category=data[levels['index']].dtypes.reset_index()
    data[make_category[make_category[0]=='int64']['index']]=data[make_category[make_category[0]=='int64']['index']].apply(pd.Categorical)
    dummies=pd.get_dummies(data[levels['index']])
    if drop==1:
        data=pd.concat([data.drop(levels['index'],axis=1) , dummies], axis=1)
    view_dtypes=data.dtypes
    data[view_dtypes[view_dtypes=='uint8'].index]=data[view_dtypes[view_dtypes=='uint8'].index].apply(lambda x: x.astype('int'))
    return data


def perform_gridsearch_xg_regression(data,predictor_name, objective_type,estimator_type,eval_metric_type):
#Perform grid search to identify optimal parameter thresholds. Metrics tested.
#some parameters have been commmented out due to their variability across 
#the mean_test_score. Needs testing on other datasets. Commented out due to 
#low impact and significant run time
#Seed set-consistent results with multiple runs
    #objective_type- examples include reg:squarederror, reg:squaredlogerror,
    #reg:logistic, binary:logistic and will depend on data and predictor format
    #estimator_type: types accepted right now are regressor or classifcation
    #Error type- squared error and can add 'reg:squaredlogerror'- squred error
    #mostly optimal check other datasets- currently commneting out
    #maximum depth of tree between 3 and 10 at 2 increments
    # min_child_weight between 1 and 6 at 2 increments
    # learning rate between .001 and .05
    # n_estimators 100 or 500
    # colsample_bytree .5, .7 or .9- no clear pattern, varies throughout
    #commenting out
    # eval_metric = Median Absolute error
    #gamma 0,1, or 10- a lot of variability across runs -commenting out
    # reg_alpha 0,1, or 10- a lot of variability across runs- commenting out
    # reg_lambda  0,1,10 
    # INPUT:
        #train: training dataset including ivs/features and dv/predictor
        #df_column_name: dv/predictor column name
    # OUTPUT:
        #best_params: list of parameters and their best thresholds
        #gridsearch.csv will show each iteration and result. Main error metric to look at is 
        #mean_test_score the higher the better model
    print('warning: This version of gridsearch is a long run. Change parameter options to reduce length (hours)')
    parameter_search =  {'nthread':[4],  
                         'objective':[objective_type],
                         "scale_pos_weight":[40, 50, 60, 70],
                         'max_depth':range(3,10,2),
                         'min_child_weight':range(1,6,2),
                         'learning_rate': [.01,.03, 0.05,.075,.1],
                         'n_estimators': [100,500],
                         #'colsample_bytree':[.5,.7,.9],
                         'eval_metric':[estimator_type],
                         #'gamma': [0,1,10],
                         #'reg_alpha':  [0,1,10],
                         'reg_lambda': [0,1,10]
                         }
    if eval_metric_type=='regressor':
    
        grid_results = GridSearchCV(estimator= XGBRegressor(seed=11),param_grid = parameter_search)

    elif eval_metric_type=='classification':
        
        grid_results = GridSearchCV(estimator= XGBClassifier(seed=11), param_grid = parameter_search)
        
    grid_results.fit(data.drop([predictor_name],axis=1),data[predictor_name])
    df = pd.DataFrame(grid_results.cv_results_)
    df.to_csv('gridsearch_' +  str(time.time()) +  '.csv')
    best_params=grid_results.best_params_
    print(best_params)
    f = open("best_parameters_gridsearch" +  str(int(time.time())) +  ".txt","w")
    f.write(str(pd.DataFrame(best_params.items())))
    f.close()
    return best_params, df

def xgboostR_best_params(best_params, x_train_data, y_train_data,x_test_data,y_test_data):
    #function will run xgboost regression model
    #INPUT
    #best_params: taken from perform_gridsearch_xg_regression function. Results of gridsearch
    #x_train_data: training dataset features/IV
    #y_train_data: training predictor
    #x_test_data: test data features/iv
    #y_test_data: predictor in test dataset
    #OUTPUT
    #mae: error metric mean absolute error
    #rmse mean squared error metric
    #score  k vs r2 results
    #error all error metrics in a dataframe
    #model model
    #y_prediction prediction results correlates with y_test
    #residuals difference between y_test -y_prediction
    model = XGBRegressor(objective =best_params['objective'], colsample_bytree = 0.5,
                learning_rate = best_params['learning_rate'],
                max_depth = best_params['max_depth'], n_estimators = best_params['n_estimators'], \
                    min_child_weight=best_params['min_child_weight'],
                reg_lambda=best_params['reg_lambda'],eval_metric=best_params['eval_metric'],nthread=best_params['nthread'])
    model.fit(x_train_data,y_train_data)
    y_prediction = model.predict(x_test_data)
    rmse = np.sqrt(mean_squared_error(y_test_data, y_prediction))
    mae = np.sqrt(mean_absolute_error(y_test_data, y_prediction))
    score=r2_score(y_test_data,y_prediction)
    plt.rcParams["figure.figsize"] = (20, 10)
    pt=plot_importance(model) 
    pt.figure.savefig('feature_importance'+ str(time.time()) + '.png')
    print('r2 score is ',score)
    print('mean_sqrd_error is==',mean_squared_error(y_test_data,y_prediction))
    print('root_mean_squared error of is==',np.sqrt(mean_squared_error(y_test_data,y_prediction)))
    print('root_mean_squared logarithmic error of is==',np.sqrt(mean_squared_log_error(y_test_data,y_prediction)))
    Error=pd.DataFrame([score, mean_squared_error(y_test_data,y_prediction), np.sqrt(mean_squared_error(y_test_data,y_prediction)),np.sqrt(mean_squared_log_error(y_test_data,y_prediction))]).T
    residuals=y_test_data-y_prediction
    return rmse, mae, score, Error, model,y_prediction,residuals

def xgboostC_best_params(best_params, x_train_data, y_train_data,x_test_data,y_test_data,boost_weights):
    #function will run xgboost Categorical model
    #INPUT
    #best_params: taken from perform_gridsearch_xg_regression function. Results of gridsearch
    #x_train_data: training dataset features/IV
    #y_train_data: training predictor
    #x_test_data: test data features/iv
    #y_test_data: predictor in test dataset
    #OUTPUT
    #error all error metrics in a dataframe
    #model model
    #y_prediction prediction results correlates with y_test
    #residuals difference between y_test -y_prediction
    model = XGBClassifier(objective =best_params['objective'], colsample_bytree = 0.5,seed=11,
                learning_rate = best_params['learning_rate'],
                max_depth = best_params['max_depth'], n_estimators = best_params['n_estimators'], \
                    min_child_weight=best_params['min_child_weight'],
                reg_lambda=best_params['reg_lambda'],eval_metric='aucpr',\
                nthread=best_params['nthread'],scale_pos_weight=boost_weights)
    model.fit(x_train_data,y_train_data)
    y_prediction = model.predict(x_test_data)
    print('Accuracy Score is==',accuracy_score(y_test_data,y_prediction))
    print('Confusion Matrix is==',confusion_matrix(y_test_data,y_prediction))
    print('Area Under Curve is==',roc_auc_score(y_test_data,y_prediction))
    print('Recall Score is==',recall_score(y_test_data,y_prediction))
    print('f1 is ==', f1_score(y_test_data,y_prediction))
    print('precision is ==', precision_score(y_test_data,y_prediction))
    print('balanced_accuracy is ==', balanced_accuracy_score(y_test_data,y_prediction))
    metrics.plot_roc_curve(model,x_test_data,y_test_data )
    Error=pd.DataFrame([ accuracy_score(y_test_data,y_prediction), \
                        confusion_matrix(y_test_data,y_prediction), \
                        roc_auc_score(y_test_data,y_prediction), \
                        recall_score(y_test_data,y_prediction), \
                        balanced_accuracy_score(y_test_data,y_prediction), \
                        precision_score(y_test_data,y_prediction), \
                        f1_score(y_test_data,y_prediction)]).T
    Error.columns=(['accuracy_score','confusion_matrix','roc_auc_score','recall_score', \
                   'balanced_accuracy_score', 'precision_score', 'f1_score'])
    Error=Error.T
    print('Roc plot made')
    f = open("ModelPerformanceError_LogReg" +  str(int(time.time())) +  ".txt","w")
    f.write(str(Error))
    f.close()
    CM=confusion_matrix(y_test_data, y_prediction)
    print(CM)
    f = open("ModelPerformanceCM_LogRegXGB" +  str(int(time.time())) +  ".txt","w")
    f.write(str(CM))
    f.close()
    residuals=y_test_data-y_prediction
    return Error, model,y_prediction,residuals

def second_deviation_zero_search(data):
    #Method to Select K in feature selection
    #Identify second deviation to find least amount of change produced by 
    #k vs R2 graph. When 10% or more of the sample is at or less than k
    #the K is chosen as the number of features. 
    #Input
    #data which is the scores variable produced by selection functions
    #which is k(y) by r2 (x) values
    #Output 
    #kk- k number of features
    #sd_plot - y is k and y is second deviation
    sd=data['x'].diff().diff()
    sd_plot=abs(pd.concat([data['y'],sd],axis=1))
    plt.plot(sd_plot['y'],sd_plot['x'])
    sd_plot_binary=(sd_plot<.0001)*1
    zero_search=[]
    for i in range(0,sd_plot_binary.shape[0]):
        zero_search.append([sd_plot_binary['x'].loc[:i].sum()/sd_plot_binary.shape[0]*100,sd_plot_binary['x'].loc[i:].sum()/sd_plot_binary.shape[0]*100])
    zero_search=pd.DataFrame(zero_search, columns=['before','after'])
    
    k_chosen=abs(zero_search['after']-20).sort_values().index[0]
    return sd_plot,zero_search, k_chosen

 
