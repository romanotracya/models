# -*- coding: utf-8 -*-
"""
Created on Tue Nov 30 19:02:32 2021

@author: roman
"""
from scipy import stats
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, roc_curve, roc_auc_score, confusion_matrix, \
    precision_score, precision_recall_curve, recall_score, f1_score,balanced_accuracy_score
from sklearn import metrics
from sklearn.metrics import f1_score
from mlxtend.feature_selection import SequentialFeatureSelector as SFS
from sklearn.model_selection import StratifiedKFold
import numpy as np
import pandas as pd 
import time 
import matplotlib.pyplot as plt
import featuretools as ft
from woodwork.logical_types import Categorical, PostalCode
from featuretools.selection import remove_highly_correlated_features, remove_highly_null_features, remove_single_value_features
#to add:
    #outlier remover with residuals
    #outlier remover for nonparameteric data
    #plot for each variable type
    #check for balance and balance if not
    
def normalize_minmax(data, id_column):
    #The following function will normalize a data matrix by column (within columns) between 
    # 0 and 1 using (x-min)/max-min
    #INPUT
    #data: dataset to be normalized. should be numeric only. predictor will be ignored with id_column
    #id_column: if id_column is present then will be skipped. Used when normalizing test datset
    #to keep ID column accuracy for kaggle submission
    #OUTPUT
    #data_norm: dataset normalized
    columns=data.columns
    newdat=[]
    for i in range(0,data.shape[1]):
        if data.columns[i]!=id_column:
            newdat.append((data.iloc[:,i]-data.iloc[:,i].min())/(data.iloc[:,i].max()-data.iloc[:,i].min()))
        else:
            newdat.append(data.iloc[:,i])
    data_norm=pd.concat(newdat,axis=1)
    return data_norm 

def run_LogReg_model(features_train,features_test, predictor_train,predictor_test, weights_model):
    #Runs logistic regression model. Will print out error information when complete and save to text file. 
    #INPUT
    #weights_model= can be best_params from gridsearch or weights from logreg_checkbinary
    #feature_train: dataset of features to run model on
    #feature_test: features dataset for test
    #predictor_train: predictor for training set
    #predictor test: predictor for test set
    #OUTPUT
    #y_prediction: prediction for test set

    if len(weights_model)==2:
       model = LogisticRegression(random_state=0,  class_weight=weights_model)
    if len(weights_model)==3:
        model = LogisticRegression(random_state=0,C=best_params['C'], class_weight=best_params['class_weight'], penalty=best_params['penalty'])
        
    # fit it
    model.fit(features_train,predictor_train)
    # test
    y_prediction = model.predict(features_test)
    print('Accuracy Score is==',accuracy_score(predictor_test,y_prediction))
    print('Confusion Matrix is==',confusion_matrix(predictor_test,y_prediction))
    print('Area Under Curve is==',roc_auc_score(predictor_test,y_prediction))
    print('Recall Score is==',recall_score(predictor_test,y_prediction))
    print('f1 is ==', f1_score(predictor_test,y_prediction))
    print('precision is ==', precision_score(predictor_test,y_prediction))
    print('balanced_accuracy is ==', balanced_accuracy_score(predictor_test,y_prediction))
    metrics.plot_roc_curve(model,features_test,predictor_test )
    Error=pd.DataFrame([ accuracy_score(predictor_test,y_prediction), \
                        confusion_matrix(predictor_test,y_prediction), \
                        roc_auc_score(predictor_test,y_prediction), \
                        recall_score(predictor_test,y_prediction), \
                        balanced_accuracy_score(predictor_test,y_prediction), \
                        precision_score(predictor_test,y_prediction), \
                        f1_score(predictor_test,y_prediction)]).T
    Error.columns=(['accuracy_score','confusion_matrix','roc_auc_score','recall_score', \
                   'balanced_accuracy_score', 'precision_score', 'f1_score'])
    Error=Error.T
    print('Roc plot made')
    f = open("ModelPerformance_LogReg" +  str(int(time.time())) +  ".txt","w")
    f.write(str(Error))
    f.close()
    return y_prediction

def run_Log_reg_model_GridSearch(features_train, predictor_train):
    #perform gridsearch for logistic regression for penalty, C, and class weight 
    #parameters. 
    #INPUT
    #features train: features training dataset
    #predictor_train: predictor for training set
    #OUTPUT
    #best_params:  to be used in logistic regression model. Best parameters based
    #on grid search
    #best_score
    model=LogisticRegression()
    weights = np.linspace(0.0,0.99,500)
    #specifying all hyperparameters with possible values
    param= {'C': [.01, 0.1, 0.5, 1,5,10], 'penalty': ['l1', 'l2'],"class_weight":[{0:x ,1:1.0 -x} for x in weights]}
    folds = StratifiedKFold(n_splits=5, shuffle=True, random_state=0)
    model= GridSearchCV(estimator= model,param_grid=param,scoring="f1",cv=folds, return_train_score=True)
    model.fit(features_train,predictor_train)
    print("Best F1 score: ", model.best_score_)
    print("Best hyperparameters: ", model.best_params_)
    best_params=model.best_params_
    best_score=model.best_score_
    return best_params, best_score


def recode_polytomous_features_frequencybased(data,predictor_name, other_features_to_group,min_levels):
    #This will create a column that reflects a categorical variable's frequency; therefore, the frequency
    #with which a level comes up for a previously existing feature becomes a new feature
    # This will occur for any feature with levels (eg. 10) or greater. This is 
    #useful for categorical varibles with many levels that may otherwise be dropped
    #INPUT
    #data: training or test dataset to be recoded
    #predictor_name: name of predictor 
    #other_features_to_group: array to be included in recoding
    #levels: how many levels must a field have to be recoded (suggestion 10 or less)
    #predictor_name
    #Format of other_features_to_group: If no additional features then set to 0 otherwise
    #  if adding feature column 1 is feature name and column 2 is 
    # Total unique values for feature or None in second column. must be a x by 2 data frame
    #other_features_to_group=pd.DataFrame(['feature',None]).T
    #OUTPUT
    #data: dataset recoded
    dtypes_list=data.drop(predictor_name,axis=1, errors='ignore').dtypes.reset_index()
    Cat_feat=pd.DataFrame(dtypes_list[dtypes_list[0]=='object']['index']).reset_index(drop=True)
    levels=data[Cat_feat['index']].apply(lambda x: len(x.unique())).reset_index()
    levels.columns=(['index','groups'])
    levels=(levels[levels['groups']>min_levels]).reset_index(drop=True)
    if other_features_to_group !=0:
        other_features_to_group.columns=(['index','groups'])
        levels=pd.concat([levels,other_features_to_group],axis=0)
    
    for i in range(0,levels.shape[0]):
        tojoin=data[levels['index'].loc[i]].value_counts().reset_index()
        data=data.merge(tojoin, left_on=levels['index'][i], right_on='index')
        data=data.drop(['index'],axis=1)
    print('What was recoded?')
    print(levels)
    return data

def recode_standardscaler(data_train,data_test):
    #The following normalization method will remove the mean and divide by the standard deviation (scaled to unit variance)
    #INPUT
    #data_train: training data without predictor variable
    #data_test: training data without id. ID is kept in test dataset to keep order 
    #OUTPUT
    #data_train: normalized training data. Add predictor back in wrapper
    #data_test: normalized test data. Add ID back in wrapper
    traincolumns=data_train.columns
    testcolumns=data_test.columns
    st_x= StandardScaler()    
    data_train= pd.DataFrame(st_x.fit_transform(data_train),columns=(traincolumns)) 
    data_test= pd.DataFrame(st_x.fit_transform(data_test),columns=(testcolumns))       
    return data_train, data_test

    
def recode_continuous_tobinned_IQR(data,predictor_name, other_features_to_group, top_q, bottom_q, min_levels):
    #This function will identify categorical variables with many levels (more than x or levels input parameter)
    #or specified features and will recode them into 3 groups based on top and bottom quartiles which are
    #specified by top_q and bottom_q
    #range.
    #INPUT
    #data: training or test dataset. should apply to both datasets
    #predictor_name: string of predictor name
    #other_features_to_group: list of features to apply function to. Must be
    #in the format shown below
    #Example other_features_to_group=pd.DataFrame(['Age',None]).T
    #top_q: quartile and up to be top bin
    #bottom_q" quartile threshold and down to be bottom bin
    #OUTPUT
    #data: dataset with recoded features having an _T at the end of the name
    dtypes_list=data.drop(predictor_name,axis=1, errors='ignore').dtypes.reset_index()
    dtypes_list=data.drop(predictor_name,axis=1, errors='ignore').dtypes.reset_index()
    dtypes_list.columns=(['index','type'])
    dtypes_list=dtypes_list[(dtypes_list['type']=='int64') | \
                                (dtypes_list['type']=='float64') | (dtypes_list['type']=='int32')]
    dtypes_list=dtypes_list[dtypes_list['index']!=predictor_name].reset_index(drop=True)
    dtypes_list=data[dtypes_list['index']].apply(lambda x: x.nunique()).reset_index()
    dtypes_list.columns=(['index','type'])
    dtypes_list=dtypes_list[dtypes_list['type']>min_levels]
    if len(other_features_to_group) !=1:
        other_features_to_group.columns=(['index','type'])
        dtypes_list=pd.DataFrame(pd.concat([other_features_to_group,dtypes_list])['index'].unique())
    for i in range(0,other_features_to_group.shape[0]):
        feature_vec=data[dtypes_list.iloc[i,0]]
        topcutoff=np.nanquantile(data[dtypes_list.iloc[i,0]],q=top_q)
        bottomcutoff=np.nanquantile(data[dtypes_list.iloc[i,0]],q=bottom_q)
        data[dtypes_list.iloc[i,0] +'_T']=pd.cut(data[dtypes_list.iloc[i,0]], \
        bins=[0,bottomcutoff, topcutoff, max(data[dtypes_list.iloc[i,0]])], labels=['low','med','high'])
    return data

def logreg_foward_backward_selection(x,y,is_step,k_change,k_max,weights_model):
    #Forward/Backward selection. Bidrectional method. 
    #Add feature one at a time and select feature wtih the minimumj p-value.  
    #iteratively create model with added features looking for the next best 
    #feature.Features added until none have minimum p-value. When a new feature
    #is added it checks other features and makes sure they still remain
    #significant. If they do not that feature is removed. 
    #INPUT
    #Weights_model
    #k_max: one way to reduce time of identifying k. Choose maximum number of 
    #features to consider
    #cv- determines the cross validation splitting strategry. default 0 splits.performance
    #on entire training set data
    #k_change: calculating the difference in r2 between each consecutive k and setting a threshold
    #for that difference. Default to .01 until further testing is performed. 
    #forward and floating determines whether using SFS, SBS, SBFS, or SFFS
    #scoring options: accuracy, f1, precision, recall, roc_auc} for classifiers,
    #{'mean_absolute_error', 'mean_squared_error'/'neg_mean_squared_error',
    #'median_absolute_error', 'r2'
    #k_features how many features are selected on iteration
    #LinearRegression() type of model 
    #x: created in wrangle_for_select def
    #y: created by wrangle_for_select def
    #OUTPUT:
    #figure: will show r2 for each k. Select k at elbow or where it plateaus
    #csv: dataframe of k column and r2 column 
    #feature_names_list: list of feature names for each k
    #: k column and r2 column
    #feature_answer: calculate determine which k change in curve is .1 or less 
    print('WARNING: This could take a long time to run. Consider using is_step if data have many features')

    if 'k_max' not in locals():
        k_max= (x.shape[1]+1)
    scores=[]
    feature_names_list=list()
    for k in range(1,k_max,is_step):
        print(k)
        sffs = SFS(LogisticRegression(),
                  k_features=k,
                  forward=True,
                  floating=True,
                  cv = 0)
        sffs.fit(x, y)
        feature_names_list.append(sffs.k_feature_names_)
        scores.append([sffs.k_score_,k])
        del sffs

    scores=pd.DataFrame(scores, columns=['x','y'])
    fig = plt.figure()
    plt.plot(scores['y'], scores['x'])
    fig.savefig('prefeature_selection_fb_feature_selection_results_MR_'+ str(time.time()) + '.png')
    plt.show()
    pd.concat([pd.DataFrame(feature_names_list),scores],axis=1).to_csv('prefeature_selection_fb_feature_selection_results_logreg_'+ str(time.time()) + '.csv')
    possible=pd.DataFrame(np.where(scores['x'].diff()<k_change))
    #find k where on a plot of score vs k, the difference between each consecutive k is .01 or less (default k_thresh). once this index is identified
    # go one step up. 
    feature_answer=int(pd.DataFrame(np.where(((scores['x'].diff()<=k_change)==False))).T.max()+1)
    feature_list=pd.DataFrame(feature_names_list[feature_answer],columns=['features'])   
    return feature_names_list,scores,feature_answer, feature_list
    
def logreg_calc_min_sample_sz(data,predictor_name):
    #Will calculate the minimum sample size. Will calculate the expected 
    #probability of the least outcome and determine size from that and number 
    #of features
    #Input
    #data: dataset that must include predictor
    #predictor_name: string with predictor name
    #Output:
    #samples
    number_of_features=data.drop([predictor_name],1).shape[1]
    min_expected=pd.DataFrame(data[predictor_name].value_counts()/data.shape[0]).min()[0]
    samplesz=round((number_of_features*10)/min_expected)
    return samplesz

def logreg_linearity(data,predictor_name, probabilities):
    #under development 
    data=data.drop([predictor_name],1)
    logit=log(probabilities/(1-probabilities))

    
def logreg_checkbinary(data,predictor_name):
    #function will print true or false. True if column is all binary and
    #false if it is not. predictor must be binary
    #Will also show whether or not predictor is balanced
    #INPUT
    #data: training data features and predictor
    #predictor_name: column name for feature located in data
    #OUTPUT
    #weights_model: this variable goes into logistic model function. 
    weights=(data[predictor_name].value_counts()/data.shape[0]*100).apply(np.floor)
    weights_model={0:int((100-weights.sort_values().reset_index(drop=True)[0])),1:int(weights.sort_values().reset_index(drop=True)[0])}
    print(data[predictor_name].isin([0,1]).all())
    print(weights)
    print(weights_model)
    weights_xgboost=len(data[data[predictor_name]==1])/len(data[data[predictor_name]==0])
    print('xgboost scale_pos_weight:', weights_xgboost)
    return weights_model,weights_xgboost
    
def get_feature_types(data):
    #Input variable to plot numeric or categorical features
    #**Method under development for identifying accuracy of dtypes **
    #INPUT
    #data: training data. 
    #OUTPUT
    #feature_types: list of features and their types according to dtypes 
    feature_types=pd.DataFrame(data.dtypes, columns=['type']).reset_index()
    return feature_types
       
def binary_vs_continuous_scatterbox(data,predictor_name,feature_types):
    #This function will identify continuous features that are int63, int32, or float and
    #and create boxplot for each predictor type and feature
    #Only features with more than 2 levels are examined. See
    #binary_vs_categorical_plot for 2 levels (binary coded features)
    #INPUT
    #data: features and predictor
    #predictor_name: column name for predictor in data 
    #feature_types: list of feature types created using dtypes and get_feature_types function
    #OUTPUT
    #output is all plotting. see directory
    feature_names=feature_types[(feature_types['type']=='int64') | \
                                (feature_types['type']=='float64') | (feature_types['type']=='int32')]
    feature=feature_names[feature_names['index']!=predictor_name].reset_index(drop=True)
    feature_names=data[feature['index']].apply(lambda x: x.nunique()).reset_index()
    feature=feature_names[feature_names[0]>2]
    if feature.shape[0] >4:
       rows= round(feature.shape[0]/3)
       columns=3
    else:
        rows=1
        columns=feature.shape[0]
    df=pd.DataFrame(pd.concat([data[feature['index']], data[predictor_name]],axis=1))
    #scatter plot
    fig, axes = plt.subplots(rows, columns,figsize=(8, 6), dpi=80)
    fig.suptitle("binary_vs_continuous_plot", fontsize=16)
    if rows>1:
        fc=0
        for r in range(0,rows):
            for c in range(0,columns):
                sns.regplot(x=feature['index'][fc], y=predictor_name, data=df, ax=axes[r][c])
                fc=(fc+2)-1
                if fc==(feature.shape[0]) :
                    break
        fig.savefig('Continuous_features_vs_binary_predictor_scatterplots'+ str(int(time.time())) + '.png')
        #boxplot
        fig.suptitle("binary_vs_continuous_plot_boxplot", fontsize=16)
        fc=0
        for r in range(0,rows):
            for c in range(0,columns):
                sns.boxplot(y=feature['index'][fc], x=predictor_name, data=df, ax=axes[r][c])
                fc=(fc+2)-1
                if fc==(feature.shape[0]) :
                    break
        fig.savefig('Continuous_features_vs_binary_predictor_boxplots'+ str(int(time.time())) + '.png')   
    if rows==1:
        for r in range(0,columns):
                sns.regplot(x=feature['index'][r], y=predictor_name, data=df, ax=axes[r])
        fig.savefig('Continuous_features_vs_binary_predictor_scatterplots'+ str(int(time.time())) + '.png')
        
        fig.suptitle("binary_vs_continuous_plot_boxplot", fontsize=16)
        for r in range(0,columns):
                sns.boxplot(y=feature['index'][r], x=predictor_name, data=df, ax=axes[r])
        fig.savefig('Continuous_features_vs_binary_predictor_boxplots'+ str(int(time.time())) + '.png')   
        
def correlation_matrix_continuous_features(data,predictor_name, min_levels):
    #function creates a correlation matrix for continous features with x levels 
    # and must be int64, float, or int32
    #INPUT
    #min_levels: minimum number of levels for a feature. Must be numeric thus greater than 
    #two to avoid binary
    #data features and predictor
    #predictor_name: column name of predictor in data
    #OUTPUT
    #output is plotting saved to directory of correlation matrix
    no2=data.apply(lambda x: x.nunique()).reset_index()
    no2_features=data[no2[no2[0]>min_levels]['index']]
    feature_types=pd.DataFrame(data.dtypes,columns=['type']).reset_index()
    feature_names=feature_types[(feature_types['type']=='int64') | \
                                (feature_types['type']=='float64') | (feature_types['type']=='int32')]
    feature=feature_names[feature_names['index']!=predictor_name].reset_index(drop=True)
    df=data[no2_features.columns.intersection(feature['index'])]
    df=pd.DataFrame(pd.concat([df, data[predictor_name]],axis=1))
    ax=plt.axes()
    fig=sns.heatmap(df.corr(),annot=True, linewidths=3)
    ax.set_title('correlation_matrix_continuous_features')
    fig = fig.get_figure()
    fig.savefig('correlation_matrix_continuous_features'+ str(int(time.time())) + '.png')
    
def continuous_hist(data,predictor_name,feature_types):
    #create histograms for continuous features. must be int64, float64
    #int32 for feature to be plotted. 
    #INPUT
    #data: features and predictor dataset
    #predictor_name: name of column in data that is the predictor
    #feature_types: made from get_feature_types function. dtypes
    #OUTPUT
    #output will be plot
    feature_names=feature_types[(feature_types['type']=='int64') | (feature_types['type']=='float64') | (feature_types['type']=='int32') ]
    feature=feature_names[feature_names['index']!=predictor_name].reset_index(drop=True)
    df=data[feature['index']]
    if feature.shape[0] >4:
       rows= round(feature.shape[0]/3)
       columns=3
    fig, axes = plt.subplots(rows, columns,figsize=(25, 20), dpi=80)
    fig.suptitle("Continuous Histograms", fontsize=16)
    fc=0
    for r in range(0,rows):
        for c in range(0,columns):
            sns.histplot(data=df,x=feature['index'][fc], ax=axes[r][c])
            plt.xlabel(feature['index'][fc])
            plt.ylabel('Frequency')
            fc=(fc+2)-1
            if fc==(feature.shape[0]) :
                break
    fig.savefig('continuous_hist_'+ str(int(time.time())) + '.png')
    
def binary_vs_categorical_plot(data,predictor_name, levels):
    #categorical variables will be viewed if there are less than x or levels (input) types within a 
    #category (likely non-preprocessed data) or if any category has 2 levels 
    #(likely coded, preprocessed data). 
    #If feature has more than 10 types, each feature will print 
    #with a frequency table and saved to a file. 
    #INPUT
    #data: features and predictor dataset
    #predictor_name: name of column for predictor
    #levels: maximum number of levels to be considered categorical or plotted for feature
    #OUTPUT
    #output will be figure saved in director
    feature_types=pd.DataFrame(data.dtypes, columns=(['type'])).reset_index()
    feature_types2=data.apply(lambda x: x.nunique()).reset_index()
    feature_names=feature_types[(feature_types['type']=='object') | (feature_types2[0]==2)]
    
    feature=feature_names[feature_names['index']!=predictor_name].reset_index(drop=True)
    length_types=(data[feature['index']].apply(lambda x: len(x.unique()))<=levels).reset_index()
    length_types=length_types[length_types[0]==True].reset_index(drop=True)
    df=pd.DataFrame(pd.concat([data[length_types['index']], data[predictor_name]],axis=1))
    if feature.shape[0] >4:
       rows= round(feature.shape[0]/3)
       columns=3
    
    fig, axes = plt.subplots(rows,columns,figsize=(30, 25), dpi=80)
    fig.suptitle("Categorical vs Binary Predictor", fontsize=15)
    i=-1                                                                     
    for r in range(0,rows):
        for c in range(0,columns):
            i=i+1
            pd.crosstab(df[length_types['index'][i]],df[predictor_name]).plot.bar(stacked=True,ax=axes[r,c], legend=False ,rot=0,fontsize=15,title=length_types['index'][i]) 
            if i >=(len(length_types['index'])-1):
                    break
    crosstab_txt=[]
    [crosstab_txt.append(pd.crosstab(df[length_types['index'][i]],df[predictor_name])) for r in range(0,rows)]
    crosstab_txt=pd.concat(crosstab_txt,axis=0)
    f = open("binary_vs_categorical_crosstab" +  str(int(time.time())) +  ".txt","w")
    f.write(str(pd.DataFrame(crosstab_txt)))
    f.close()
    fig.savefig('binary_vs_categorical_crosstab'+ str(int(time.time())) + '.png')

def expand_features(data,predictor_name, id_column_name,predictor_column, id_column):
    #function will create features based on numeric only table after transformations. 
    #performs feature transformations on data table using featuretools
    #data: final dataset after preprocessing where all features are normalized. Do not inlucde the
    #predictor. if test data do not include id column.  
    #id_column_name: the column name if test set is being added if not set to 0
    #predictor_name: the predictor column name if not present set to 0
    #id_column: id column so can be added back after transformations if none then 0
    #predictor_column predictor column so can be added back after transformations if none then 0
    data=data.drop(predictor_name, axis=1, errors='ignore')
    data=data.drop(id_column, axis=1, errors='ignore')
    es = ft.EntitySet(id = 'dataset')
    es = es.add_dataframe(
        dataframe=data,
        dataframe_name="data_new",
        index="index"
    )
    # Run deep feature synthesis with transformation primitives
    feature_matrix, features = ft.dfs(entityset = es, target_dataframe_name = "data_new",
                                          trans_primitives = ['add_numeric', 'multiply_numeric'])
    feature_matrix=ft.selection.remove_highly_null_features(feature_matrix, pct_null_threshold=1) #remove 100% of nulls
    feature_matrix, features = remove_single_value_features(feature_matrix, features=features) #remove features without variance 
    feature_matrix, features  = remove_highly_correlated_features(feature_matrix, features=features, pct_corr_threshold=.8) #remove highly correlated features
    print(feature_matrix.head())
    if np.isscalar(predictor_column)==False:
        data=pd.concat([feature_matrix, predictor_column], axis=1)
    
    elif np.isscalar(id_column)==False:
        data=pd.concat([feature_matrix, id_column], axis=1)
    return data, features 