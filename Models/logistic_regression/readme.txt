This logistic regression code is meant to be a baseline or starting place for any logistic regression model. 

These datafiles were created based on the kaggle dataset -  Titanic-Machine Learning from Disaster, 
but the code was designed to be applied to any train/test dataset. 
Link: https://www.kaggle.com/c/titanic

This model is under development and the files are not yet runable. This readme.txt will be updated when the base
model is complete.

