# -*- coding: utf-8 -*-
"""
Created on Tue Nov 30 19:01:53 2021

@author: roman
"""

#dataset being used is Titanic- Kaggle 
#kaggle competitions download -c titanic
#link: https://www.kaggle.com/c/titanic/data

#Assumptions
#binary predictor/iv
#Independent observations- no repeated measures
# no multicollinearity amoung IVs/features
#linearity of IV/features with log odds  

import os
import pandas as pd
import numpy as np
import math 
from sklearn.model_selection import train_test_split
import seaborn as sns
from sklearn.preprocessing import StandardScaler   
exec(open('C:/Users/roman/Documents/Clydesdale/models/logistic_regression/logreg_model_building.py').read())
exec(open('C:/Users/roman/Documents/Clydesdale/models/multiple_regression/multiple_regression_model_building.py').read())
exec(open('C:/Users/roman/Documents/Clydesdale/models/xgboost/xgboost_model_building.py').read())
#kaggle api: kaggle competitions download -c house-prices-advanced-regression-techniques
#https://www.kaggle.com/c/house-prices-advanced-regression-techniques/data
os.chdir('C:/Users/roman/Documents/Clydesdale/models/logistic_regression')
train=pd.read_csv('train.csv')
test=pd.read_csv('test.csv')
predictor_name='Survived'
#drop ID column
train=train.drop(['PassengerId'],1)
#drop nulls if predictor
train=train[(train[predictor_name].isnull())==False]
#test=test.drop(['PassengerId'],1)

#Extract last name and identify groups. Code as group.
train['last_name']=train['Name'].apply(lambda x: x.split(',')[0])
test['last_name']=test['Name'].apply(lambda x: x.split(',')[0])

#Creating Features ------ Traveling with Children------- Miss, Mr, Mrs, Miss--- recode to binary
#If group and female label

train=train.merge(train['last_name'].value_counts().reset_index(), how='left', left_on='last_name',right_on='index')
train['number_in_group']=train['last_name_y']
train=train.drop(['last_name_x','index','last_name_y'],axis=1)


test=test.merge(test['last_name'].value_counts().reset_index(), how='left', left_on='last_name',right_on='index')
test['number_in_group']=test['last_name_y']

test=test.drop(['last_name_y','index','last_name_x'],axis=1)

#If group has child then label
train['Under2']=(((train['number_in_group']>1 ) & (train['Age']<=2))*1)
train['Under12']=(((train['number_in_group']>1 ) & (train['Age']<=12))*1)
train['Under18']=(((train['number_in_group']>1 ) & (train['Age']<=18))*1)
train['Sibling']=(( (train['SibSp']>0))*1)


test['Under2']=(((test['number_in_group']>1 ) & (test['Age']<=2))*1)
test['Under12']=(((test['number_in_group']>1 ) & (test['Age']<=12))*1)
test['Under18']=(((test['number_in_group']>1 ) & (test['Age']<=18))*1)
test['Sibling']=(( (test['SibSp']>0))*1)


#Miss, mr, mrs
train['Miss']=(train['Name'].str.contains('Miss'))*1
train['Mr']=(train['Name'].str.contains('Mr'))*1
train['Mrs']=(train['Name'].str.contains('Mrs'))*1
train['Master']=(train['Name'].str.contains('Master'))*1

test['Miss']=(test['Name'].str.contains('Miss'))*1
test['Mr']=(test['Name'].str.contains('Mr'))*1
test['Mrs']=(test['Name'].str.contains('Mrs'))*1
test['Master']=(test['Name'].str.contains('Master'))*1
######################################################################################
#drop if more than 70% of feature missing
print('are there any features that have more than 70% data missing?')
too_many_missing, missing_pct=drop_var_missing(train)
train=train.drop(['Cabin'],axis=1)
test=test.drop(['Cabin'],axis=1)

train=train.drop(['Name'],axis=1)
test=test.drop(['Name'],axis=1)

####################Impute missing values ########################################
#impute nulls with mean
train_prepro=impute_median(train)
test_prepro=impute_median(test)

#impute with mode
train_prepro=impute_mode(train_prepro)
test_prepro=impute_mode(test_prepro)

##############################Recoding features ###############################

#recode features categorical/multiple levels (>10) convert to frequency of occurance
train_prepro=recode_polytomous_features_frequencybased(train_prepro,predictor_name, other_features_to_group=0,min_levels=10)
test_prepro=recode_polytomous_features_frequencybased(test_prepro,predictor_name, other_features_to_group=0,min_levels=10)
#drop ticket due to recoding
train_prepro=train_prepro.drop(['Ticket_x'],axis=1)
test_prepro=test_prepro.drop(['Ticket_x'],axis=1)

#add new potential variable for age and fare where grouped in quartiles in addition to specific ages
#other_features_to_group=pd.DataFrame(['Age',None]).T
other_features_to_group=pd.DataFrame([['Age',None]])
train_prepro=recode_continuous_tobinned_IQR(train_prepro,predictor_name, other_features_to_group,top_q=.95,bottom_q=.05,min_levels=10)
test_prepro=recode_continuous_tobinned_IQR(test_prepro,predictor_name, other_features_to_group,top_q=.95,bottom_q=.05,min_levels=10)

other_features_to_group=pd.DataFrame([['Fare', None]])
train_prepro=recode_continuous_tobinned_IQR(train_prepro,predictor_name, other_features_to_group,top_q=.95,bottom_q=.05,min_levels=10)
test_prepro=recode_continuous_tobinned_IQR(test_prepro,predictor_name, other_features_to_group,top_q=.95,bottom_q=.05,min_levels=10)


#Recode categorical to binary if under 10 levels
train_prepro= recode_to_binary(train_prepro, predictor_name, code_all=10,drop=1)
test_prepro= recode_to_binary(test_prepro,predictor_name, code_all=10,drop=1)

################Train/Test Consistency#########################################

#make sure train/test columns match. Once have column names post-recode
storetestid=test_prepro['PassengerId']
storetrainp=train_prepro['Survived']
train_prepro=train_prepro[train_prepro.columns.intersection(test_prepro.columns)]
test_prepro=test_prepro[train_prepro.columns.intersection(test_prepro.columns)]


######################Plotting and exploration of features#######################

#examine data- histogram, scatterplots, correlation matrix, boxplots, and crosstabs of features depending on type
feature_types_prepro=get_feature_types(train)
feature_types=get_feature_types(train)
binary_vs_continuous_scatterbox(train,predictor_name,feature_types_prepro)
continuous_hist(train,predictor_name,feature_types) #before coding
correlation_matrix_continuous_features(pd.concat([train_prepro, storetrainp],axis=1),predictor_name,min_levels=2) #after coding
correlation_matrix_continuous_features(train,predictor_name,min_levels=10) #before coding
binary_vs_categorical_plot(pd.concat([train_prepro, storetrainp],axis=1),predictor_name,levels=10) #use train due to categorical features already recoded

##############Normalize dataset #############################################

#normalize all data between 0 and 1
id_column='PassengerId'
#train_prepro=normalize_minmax(train_prepro,id_column)
#test_prepro=normalize_minmax(test_prepro,id_column)

train_prepro, test_prepro=recode_standardscaler(train_prepro,test_prepro)

train_prepro=pd.concat([train_prepro, storetrainp],axis=1)
test_prepro=pd.concat([train_prepro, storetestid],axis=1)
##############N Identify Weights #############################################

#check if binary and return weights for model
weights,weights_xgboost=logreg_checkbinary(train_prepro,predictor_name)
#Imbalance where class 1 is only 40% . Bias towards 0- not survived. 

##############Setup for Cross-validation#######################################

#split training data, and clean
x_train, x_test, y_train, y_test=train_test_split(train_prepro.iloc[:,0:train_prepro.shape[1]-1],train_prepro[predictor_name],test_size=.3, random_state=0)
x_train_index, y_train_index, x_train_pro, y_train_pro, x_test_pro, y_test_pro= store_index(x_train, x_test, y_train, y_test)

##############Run Model Base #############################################
#rerun model with correct features 
features_train=x_train_pro.reset_index(drop=True)
predictor_train=y_train_pro
predictor_test=y_test_pro
features_test=x_test_pro.reset_index(drop=True)
features_test=features_test[features_train.columns]
#This model is predicting with out feature selection 
run_LogReg_model(features_train,features_test, predictor_train,predictor_test, weights)

##############Adjust for multicoliniarity #############################################
x_train_pro, vif_data=vif_multicollin_remove(x_train_pro, y_train_pro,predictor_name, vif_thresh=20)
x,y=wrangle_for_select(x_train_pro, y_train_pro,predictor_name)
#feature selection

##############Feature Selection  #############################################
#find curve, shorten loop, increase granularity to look for elbow 
feature_names_list,scores,feature_answer,feature_list=logreg_foward_backward_selection(x,y,is_step=1,k_change=.01, k_max=x.shape[1],weights_model=weights)

##############Examine Feature Selection #############################################
#see model description for each feature grouping
for i in range(0,len(feature_names_list)):
    print(i)
    features_train=x_train_pro[pd.DataFrame(feature_names_list[i])[0]].reset_index(drop=True)
    predictor_train=y_train_pro.reset_index(drop=True)
    predictor_test=y_test_pro
    features_test=x_test_pro[pd.DataFrame(feature_names_list[i])[0]].reset_index(drop=True)
    features_test=features_test[features_train.columns]
    #This model is predicting with feature selection 
    run_LogReg_model(features_train,features_test, predictor_train,predictor_test, weights_model=weights)
    
##############Rerun Model with Features #############################################
number_of_features=15 #chosen from plot from selection function 

features_train=x_train_pro[pd.DataFrame(feature_names_list[number_of_features])[0]].reset_index(drop=True)
predictor_train=y_train_pro.reset_index(drop=True)
predictor_test=y_test_pro
features_test=x_test_pro[pd.DataFrame(feature_names_list[number_of_features])[0]].reset_index(drop=True)
features_test=features_test[features_train.columns]
#This model is predicting with feature selection 
run_LogReg_model(features_train,features_test, predictor_train,predictor_test, weights)

##############Grid Search #############################################
#Grid search to improve model score 
best_params, best_score=run_Log_reg_model_GridSearch(features_train, predictor_train)
run_LogReg_model(features_train,features_test, predictor_train,predictor_test, weights_model=best_params)

run_LogReg_model(features_train,features_test, predictor_train,predictor_test,  weights_model={0: 0.4840881763527054, 1: 0.5159118236472946})


features_train=x_train_pro[pd.DataFrame(feature_names_list[number_of_features])[0]].reset_index(drop=True)
predictor_train=y_train_pro.reset_index(drop=True)
predictor_test=y_test_pro
features_test=x_test_pro[pd.DataFrame(feature_names_list[number_of_features])[0]].reset_index(drop=True)
features_test=features_test[features_train.columns]
run_LogReg_model(features_train,features_test, predictor_train,predictor_test,  weights_model=best_params)

##############Kaggle test set to submit#########################################
#test=pd.read_csv('test.csv')

model = LogisticRegression(random_state=0, class_weight=weights)
 # fit it
model.fit(features_train,predictor_train)
y_prediction = model.predict(features_test)
print('Accuracy Score is==',accuracy_score(predictor_test,y_prediction))
print('Confusion Matrix is==',confusion_matrix(predictor_test,y_prediction))
print('Area Under Curve is==',roc_auc_score(predictor_test,y_prediction))
print('Recall Score is==',recall_score(predictor_test,y_prediction))
print('f1 is ==', f1_score(predictor_test,y_prediction))
print('precision is ==', precision_score(predictor_test,y_prediction))
print('balanced_accuracy is ==', balanced_accuracy_score(predictor_test,y_prediction))
# test
features_test=test_prepro[pd.DataFrame(feature_names_list[number_of_features])[0]].reset_index(drop=True)
features_test=features_test[features_train.columns]
y_prediction = model.predict(features_test)
Results=pd.concat([test_prepro['PassengerId'],pd.DataFrame(y_prediction.astype(int))],axis=1)
Results=Results.sort_values('PassengerId').reset_index(drop=True)
Results.columns=['PassengerId','Survived']



