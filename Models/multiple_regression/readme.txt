This multiple regression code is meant to be a baseline or starting place for any multiple regression model. 

These datafiles were created based on the kaggle dataset -  house-prices-advanced-regression-techniques, 
but the code was designed to be applied to any train/test dataset. 

Two primary scripts:

1) multiple_egression_model_building.py: contains functions needed to run the multiple regression model, 
including preprocessing and model evaluation. Called by runfile.py. 

General description of functions include:
outlier detection and possible removal
other data cleaning (nulls, data type) removes all objects/categorical,imputing mean in NAs
data Transformations
test/train dataset development 
feature selection-forward/backward/bi selection processes
multicollinearity checks- vif and correlation
checks and address linearity assumption
residual analysis and model confirmation 

2) runfile.py: Runs the model. This file is specific to the dataset under investigation.  Link to data on
kaggle provided in runfile.py. 


Summary and Notes:
All categorical data are removed given Multiple Regression requires numeric datasets. In the future, 
some fields or new metrics can be made to improve this particular dataset and likely future datasets. Only numeric
features analyzed and no re-coding(eg. one hot encoding) included yet.  


Model performance:
r2= .74
Residuals mostly randomly  distributed and linear relationship seen on actual and predicted- see plot
All plots and csv files produced during model run are uploaded to gitlab and found in models folder. 

final:
next model xgboost where similar process can be done on all features- neither limited by numeric datatype nor 
linearity. 
. 
