# -*- coding: utf-8 -*-
"""
Created on Sun Nov 21 16:41:39 2021

@author: roman
"""

import os
import pandas as pd
import numpy as np
import math 
from sklearn.model_selection import train_test_split
import seaborn as sns
from statsmodels.stats.outliers_influence import variance_inflation_factor
from mlxtend.feature_selection import SequentialFeatureSelector as SFS
from sklearn.linear_model import LinearRegression  
import matplotlib.pyplot as plt
from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error
from sklearn.impute import SimpleImputer
import time
os.chdir('/users/roman/documents/clydesdale/Models/multiple_regression')
exec(open('multiple_regression_model_building.py').read())
#kaggle api: kaggle competitions download -c house-prices-advanced-regression-techniques
#https://www.kaggle.com/c/house-prices-advanced-regression-techniques/data
#os.chdir('')
train=pd.read_csv('train.csv')
test=pd.read_csv('test.csv')
#drop ID column
train=train.drop(['Id'],1)
test=test.drop(['Id'],1)
predictor_name='SalePrice'

#remove any categorical variables
train=train[train.dtypes[train.dtypes!= object].index]

#remove outlier with std greater than 3 and drop columns if more than 70% missing
train=outlier_remove(train, 3) 

#check if there are any features with mostly missing (>70%)
too_many_missing, missing_pct=drop_var_missing(train)

#split training data, and clean
x_train, x_test, y_train, y_test=train_test_split(train.iloc[:,0:train.shape[1]-1],train['SalePrice'],test_size=.3, random_state=0)
x_train_index, y_train_index, x_train_pro, y_train_pro, x_test_pro, y_test_pro= store_index(x_train, x_test, y_train, y_test)


#remove any columns with correlation less than .2 with DV/predictor
correlated_columns, correlation_values,x_train_pro, x_test_pro,uncorrelated_columns=remove_low_corr(x_train_pro, x_test_pro,y_train_pro, predictor_name,corr_thresh=.2)
method_type=1 #log transform done 
linear_correlation_threshold=.2

#linear transform dropped features and add if correlation improved
transformed_features, transform_corrmatrix, method_type, x_train_pro=try_linear_transform(x_train, y_train,x_train_pro,predictor_name,uncorrelated_columns,method_type)   

#see if multicollinearity between pairs of features and remove if present 
x_train_pro, multico_features, unique_drop=find_multicollinearity(x_train_pro,y_train_pro, predictor_name)
# test multicollinearity using vif- test groups not just pairs drop if present
x_train_pro, vif_data=vif_multicollin_remove(x_train_pro, y_train_pro,predictor_name,vif_thresh=15)

#feature selection
x,y=wrangle_for_select(x_train_pro, y_train_pro,predictor_name)
feature_names_list,scores,feature_answer,feature_list=foward_backward_selection(x,y,is_step=1,k_change=.01, k_max=60)
sd_plot,zero_search, k_chosen=second_deviation_zero_search(data=scores)
check_selection(feature_names_list, k_chosen)
#more preprocessing
transform_matrix=try_transform(x_train_pro, y_train_pro, predictor_name,method_type)   
y_dataT=transform_matrix[predictor_name]
x_dataT=transform_matrix.drop([predictor_name],1)

transform_matrix=try_transform(x_test_pro, y_test_pro, predictor_name,method_type)   
y_testT=transform_matrix[predictor_name]
x_testT=transform_matrix.drop([predictor_name],1)

x_train_pro_fs,x_test_pro_fs=create_dataset_from_fs(feature_names_list,x_dataT,x_testT, index=k_chosen,predictor_name='SalePrice') 
x_train_pro_fs=impute_mean(x_train_pro_fs)
x_test_pro_fs=impute_mean(x_test_pro_fs)
x_test_pro_fs=x_test_pro_fs[x_train_pro_fs.columns]
x_data=x_train_pro_fs
y_data=y_dataT
x_test=x_test_pro_fs
#run model
y_test, x_test, residuals2, y_prediction, LR,Error_train, Error_test =run_MR_model(x_train_pro_fs,x_test_pro_fs, y_testT,y_data) 

#plot residuals
plot_residuals(y_prediction, residuals2, y_test)
#scatter plots for each IV/features and DV/predictor in training dataset 
scatter_corr(x_train_pro_fs, y_train_pro, 'scatter_plots_features_predictors')


