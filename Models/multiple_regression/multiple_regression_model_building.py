# -*- coding: utf-8 -*-
"""
Created on Fri Nov 19 19:45:34 2021

@author: roman
"""

import os
import pandas as pd
import numpy as np
import math 
from sklearn.model_selection import train_test_split
import seaborn as sns
from statsmodels.stats.outliers_influence import variance_inflation_factor
from mlxtend.feature_selection import SequentialFeatureSelector as SFS
from sklearn.linear_model import LinearRegression  
import matplotlib.pyplot as plt
from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error
from sklearn.impute import SimpleImputer
import time



def outlier_remove(data, std_value):
    #this function will calculate the standard deviation of each column and remove any row with an outlier 
    #above or below x std from the mean. 
    # Input
    #data: dataset with outliers to be removed
    #std_value: threshold value. number of standard deviations from the mean that will define an outlier. 
    #output
    #data: input dataset where outliers have been removed
    for i in range(0,data.shape[1]):
       outlier_idx=pd.DataFrame(np.where(abs(data.iloc[:,i])>(data.iloc[:,i].mean()+(data.iloc[:,i].std()*std_value))))
       data=data.drop(outlier_idx,axis=0).reset_index(drop=True)
    return data

def drop_var_missing(data):
    #drop any column that has more than 70% of the datapoints missing. Will also print file
    #Variables and the percent missing. 
    #INPUT:
    #data: any dataframe. will go column by column and count number missing
    #OUTPUT:
    #Too_many_missing: data with missing columns removed
    #missing_pct: will be a text file and output. Shows percent missing of all features in dataset
    missing_pct=(data.apply(lambda x: x.isnull())*1).sum(axis=0)/(data.shape[0])*100
    print(missing_pct)
    f = open("missing_features_pcts" +  str(int(time.time())) +  ".txt","w")
    f.write(str(pd.DataFrame(missing_pct.items())))
    f.close()
    too_many_missing=data.columns[np.where((data.apply(lambda x: x.isnull())*1).sum(axis=0)/(data.shape[0])*100>70)]
    if len(data)>0:
        print('warning: identified feature has too many missing')
        print(too_many_missing)
    return too_many_missing, missing_pct 


def store_index(x_train, x_test, y_train, y_test):
    #make sure all indices are in sequential order. to be used after test and training sets are created. This 
    #test data is not the testing set from kaggle. It is the training set divided into test and train
    #Input:
    #x_train: IVs of training set data
    #y_train: dv or predictor of training set
    #x_test: test set IVs
    #y_test: test set predictor or DV
    #Output
    #x_train_index: Index of x_train from the original dataframe
    #y_train_index: index of y_train from the original dataframe
    #x_train_pro: x_train/ivs dataframe to be used for subsequent analysis with ordered index
    #y_train_pro: y_train/predictor to be used for subsequent analysis with ordered index
    #x_test_pro: iv/test data to be used for subsequent analysis wtih ordered index
    #y_test_pro: dv/test data to be used for subsequent analyssi wtih ordered indices
    x_train_index=x_train.index
    y_train_index=y_train.index
    x_train_pro=x_train.reset_index(drop=True)
    y_train_pro=y_train.reset_index(drop=True)
    x_test_pro=x_test.reset_index(drop=True)
    y_test_pro=y_test.reset_index(drop=True)
    return x_train_index, y_train_index, x_train_pro, y_train_pro, x_test_pro, y_test_pro


def remove_low_corr(x_train_pro, x_test_pro,y_train_pro, predictor_name,corr_thresh):
    #method to measure linearity between each IV with the DV. IVs will be 
    #dropped when low correlation is present. #only training will be examined for 
    #low correlations, but results will be applied to train and test sets. 
    #input
    #x_train_pro: train/ivs will calculate correlation of each IV with Predictor
    #any correlation below corr thresh will be dropped from train and test data
    #x_test_pro: test data/IVs results from x_train_pro will be applied to this dataset
    #y_train_pro: train/predictor. variable training/iv will be correlated wtih
    #corr_thresh: threshold where anything below value will be dropped 
    #predictor_name- column name for predictor
    #Output:
    #correlated_columns: list of columns that are correlated =>.2
    #uncorrelated_columns: list of columns that will be dropped due to low correlation
    #correlated values: list of correlation values for all variables to dv
    IVDV=pd.concat([x_train_pro, y_train_pro],axis=1)
    correlated_columns=IVDV.columns[[abs(IVDV.corr()[predictor_name])>=corr_thresh]]
    uncorrelated_columns=IVDV.columns[[abs(IVDV.corr()[predictor_name])<corr_thresh]]
    correlation_values=IVDV[correlated_columns].corr()[predictor_name]
    correlated_columns=correlated_columns.drop([predictor_name],1)
    if 'x_train_pro' in locals():
        x_train_pro=x_train_pro[correlated_columns]
    else: 
        x_train_pro=math.nan
    if 'x_test_pro' in locals():
        x_test_pro=x_test_pro[correlated_columns]
    else:
        x_test_pro=math.nan
    return correlated_columns, correlation_values,x_train_pro, x_test_pro,uncorrelated_columns



def try_linear_transform(x_train, y_train,x_train_pro, predictor_name,uncorrelated_columns,method_type):
    # One option in transformation is to only transform features that have low 
    #correlation with the DV.  This function will try linear transform on low 
    #correlation variables to see if can be added back
    #Input:
    #uncorrelated_columns: taken from remove_low_corr function. List of columns
    #dropped due to low correlation will be tranformed here and retested
    #x_train: IVs/training data before any preprocessing where features were dropped
    #tranform will be applied on uncorrelated features from this dataframe
    #y_train: dv/predictor used to correlated to
    #x_train_pro: preprocessed data columns that reach correlation threshold
    #due to transformation will be added back into this dataframe
    #if method type is 1 will do log transform with a 100 constant. 
    #if method type is not 1 will do a square root transformation
    #Output:
    #transformed_features: dataframe of features that were transformed. empty if none
    #transform_corrmatrix: the correlation matrix used to evaluate the transformed features
    #method_type: defines the method of transformation
    #x_train_pro: dataframe with added transformed features if any
    IVDV=pd.concat([x_train, y_train],axis=1)
    if method_type==1:
        transform_matrix=pd.concat([(IVDV[uncorrelated_columns]+100).apply(lambda x: np.log(x),axis=1).round(4), y_train],axis=1)
    else:
        transform_matrix=pd.concat([(IVDV[uncorrelated_columns]).apply(lambda x: np.sqrt(x),axis=1).round(2), y_train],axis=1).corr().drop([predictor_name],axis=0)
    transform_corrmatrix=transform_matrix.corr().drop([predictor_name],axis=0)
    if np.where(abs(transform_corrmatrix[predictor_name])>=linear_correlation_threshold)[0].size!=0:
        transformed_features=transform_matrix[transform_corrmatrix.index[np.where(abs(transform_corrmatrix[predictor_name])>=linear_correlation_threshold)]]
        x_train_pro=pd.concat([x_train_pro,transformed_features],axis=1)
    else:
        transformed_features=[]
    return transformed_features, transform_corrmatrix, method_type,x_train_pro    
        

def find_multicollinearity(x_data, y_data, predictor_name):
    #will look for IVs that are correlated with each other. Pairs only vif 
    #will look at features that correlate as groups. If two IVS are correlated
    #the IV with the lowest correlation to the DV will be dropped. 
    #Input:
    #x_data: IV/training preprocessed data that will be examined for correlated pair
    #y_data: predictor/DV preprocessed data
    #Output:
    #multico_features: dataframe of features that are correlated with eachother a column for
    #kept features and column for corresponding dropped features 
    #unique_drop: list of features that are dropped 
    #x_train_pro: dataframe with dropped features
    keep_features_multico=[]
    drop_features_multico=[]
    IVDV=pd.concat([x_data, y_data],axis=1)
    corr_matrix=IVDV.corr()
    for i in range(0,IVDV.shape[1]-1):
         get_highcorr=pd.DataFrame(corr_matrix.drop(['SalePrice'],axis=1).index[(np.where(corr_matrix.drop([predictor_name],axis=1).iloc[:,i]>=.8))])
         corr_matrix_idx=corr_matrix.reset_index()
         if get_highcorr.shape[0]>1:
             keep_feature=corr_matrix_idx.reset_index().merge(get_highcorr, how='inner', left_on='index', right_on=0).groupby(['index'])[predictor_name].max().sort_values(ascending=False).index[0]
             drop_feature=corr_matrix_idx.reset_index().merge(get_highcorr, how='inner', left_on='index', right_on=0).groupby(['index'])[predictor_name].max().sort_values(ascending=False).index[1]
         else:
             keep_feature=np.nan
             drop_feature=np.nan
         keep_features_multico.append(keep_feature)
         drop_features_multico.append(drop_feature)
         del keep_feature, get_highcorr, corr_matrix_idx
    keep_features_multico=pd.DataFrame(keep_features_multico,columns=['keep']).dropna()
    drop_features_multico=pd.DataFrame(drop_features_multico,columns=['drop']).dropna()
    multico_features=pd.concat([keep_features_multico, drop_features_multico],axis=1)
    unique_drop=pd.unique(drop_features_multico['drop'])
    x_data.drop(unique_drop[[range(0, len(unique_drop))]],axis=1, inplace=True)
    return x_data, multico_features, unique_drop
 

def vif_multicollin_remove(x_data, y_data,predictor_name, vif_thresh):
    #Variance inflation factor: will remove any features with a vif greater than 
    #threshold. Will remove the highest then recalculate and will iterate in while loop
    #until all features left are under threshold 
    
    #Input:
    #x_data:  dataframe with IVs/features to be analyzed with vif
    #y_data: predictor, but not used
    #predictor_name: dv but not used
    #vif_thresh default to 15 or hypertune
    #Output:
    #x_data: preprocessed dataframe where features with high vif are removed
    #vif_data: final list of eatures and their vif
    vif_solution=20
    IVDV=pd.DataFrame(pd.concat([x_data, y_data],axis=1).dropna())
    IVDV=IVDV.drop(predictor_name,axis=1)
    include=IVDV[IVDV.columns[IVDV.dtypes.astype(str).isin(['int64','float64','int32'])]]
    disclude=IVDV[IVDV.columns[~IVDV.dtypes.astype(str).isin(['int64','float64','int32'])]]
    include=include.loc[:, (include.sum(axis=0) != 0)] #drop columns that sum to 0
    while vif_solution>1 :
        vif_data=pd.DataFrame()
        vif_data["feature"] = include.columns
        vif_data["VIF"] = [variance_inflation_factor(include.values, i)
                           for i in range(len(include.columns))]
        ToDrop=pd.DataFrame(vif_data.sort_values('VIF',ascending=False).reset_index(drop=True).loc[0]).T
        vif_solution=((vif_data['VIF']>=vif_thresh)*1).sum() + (np.isinf(vif_data['VIF'])*1).sum()
        if (pd.DataFrame(ToDrop['VIF']>vif_thresh)['VIF'][0])==True:
            include=include.drop(ToDrop['feature'],axis=1)
            vif_dropped=pd.DataFrame(vif_data.sort_values('VIF',ascending=False).reset_index(drop=True).loc[0]).T
            print(vif_data)
            print('any dropped?')
            print(vif_dropped)
            print(vif_solution)
    x_data= pd.concat([include, disclude],axis=1)
    return x_data, vif_data

def run_vif_loop(mc_dat,vif_thresh):
    #this function will take dataset of features and perform a loop to reduce to 
    #features with multicolliniarity belowthreshold. This function is currently 
    #embedded in vif_multicollin_remove_extended
    vif_solution=20
    while vif_solution>1 :
        vif_data=pd.DataFrame()
        vif_data["feature"] =mc_dat.columns
        vif_data["VIF"] = [variance_inflation_factor(mc_dat.values, i)
                           for i in range(len(mc_dat.columns))]
        ToDrop=pd.DataFrame(vif_data.sort_values('VIF',ascending=False).reset_index(drop=True).loc[0]).T
        vif_solution=((vif_data['VIF']>=vif_thresh)*1).sum() + (np.isinf(vif_data['VIF'])*1).sum()
        if (pd.DataFrame(ToDrop['VIF']>vif_thresh)['VIF'][0])==True:
            mc_dat=mc_dat.drop(ToDrop['feature'],axis=1)
            vif_dropped=pd.DataFrame(vif_data.sort_values('VIF',ascending=False).reset_index(drop=True).loc[0]).T
    return mc_dat,vif_data
      
def vif_multicollin_remove_extended(x_data, y_data,predictor_name, vif_thresh):
    #Variance inflation factor: will remove any features with a vif greater than 
    #threshold. This function is meant to be used with datasets that have many features >50
    #or have a slow running time due to the amount of features checked (featuretools).
    #Starts with first set of features that are not created through transformation (eg. X or + 
    #of two or more features),
    #calcualtes the vif, removes the highest until they are all below threshold
    #then adds 30 more features (randomaly selected on each iteration) 
    #to the set already run, calculates the vif on old and new sets and this continues
    #until there are no features left. This speeds up the processing time 
    #when comparing to running all features at once due to the reduction in features
    #with each run.
    #Example of processing time: 623 X 904 matrix took 5 hrs
    #Input:
    #x_data:  dataframe with IVs/features to be analyzed with vif
    #y_data: predictor, but not used
    #predictor_name: dv but not used
    #vif_thresh default to 15 or hypertune
    #Output:
    #x_data: preprocessed dataframe where features with high vif are removed
    #vif_data: final list of eatures and their vif
    vif_solution=20
    IVDV=pd.DataFrame(pd.concat([x_data, y_data],axis=1).dropna())
    IVDV=IVDV.drop(predictor_name,axis=1)
    include=IVDV[IVDV.columns[IVDV.dtypes.astype(str).isin(['int64','float64','int32'])]]
    disclude=IVDV[IVDV.columns[~IVDV.dtypes.astype(str).isin(['int64','float64','int32'])]]
    include=include.loc[:, (include.sum(axis=0) != 0)] #drop columns that sum to 0
    
    # perform vif on 30 new columns at a time that are randomly sampled
    FirstSet= x_data.iloc[:,0:train.shape[1]].shape[1]
    FirstSet= max([30, FirstSet]) #make sure first group is original features without featuretools
    PartA=[1]*FirstSet
    n=round(train_prepro.shape[1]/FirstSet)-1 
    groups = range(0,n+1)
    a = np.repeat(groups,int(train_prepro.shape[1]/n))+2
    random.shuffle(a)
    group_list=pd.concat([pd.DataFrame(PartA), pd.DataFrame(a)],axis=0)
    group_list=group_list[0:include.shape[1]]
    datout=[]
    print('Total Number of groups')
    print(max(group_list[0]))
    for g in range(1,max(group_list[0])):
        print('current group run:')
        print(g)
        mcdat=include.loc[:,np.array(group_list[0]==g)]
        vec, vif_data=run_vif_loop(pd.concat([pd.DataFrame(datout), mcdat],axis=1),vif_thresh=15)
        datout=vec
    x_data= pd.concat([datout, disclude],axis=1)        
    return x_data, vif_data


def wrangle_for_select(x_train_pro, y_train_pro, predictor_name):
    #this function creates datasets to be used with three functions
    #that perform forward/backward/bidirectional feature selection. 
    #Input:
    #x_train_pro: preprocessed features/iv
    #y_train_pro: preprocessed dv/predictor
    #output: 
    #x: features/iv na's dropped and dv removed
    #y: dv/predictor matching list given dropped na's from x
    merge_train=pd.concat([x_train_pro, y_train_pro],axis=1)
    merge_train=merge_train.dropna().reset_index(drop=True)
    x=merge_train.drop([predictor_name],axis=1)
    y=merge_train[predictor_name]    
    return x,y    


#Bidirectionalelimination (Step Wise Selection)
def forward_selection_method(x,y,is_step,k_change,k_max):
    #Forward Selection
    #Add feature one at a time and select feature wtih the minimumj p-value.  
    #iteratively create model with added features looking for the next best 
    #feature.Features added until none have minimum p-value. 
    #Input
    #k_max: one way to reduce time of identifying k. Choose maximum number of 
    #features to consider
    #is_step: increments of k. value should be larger as variable size increases 
    #cv- determines the cross validation splitting strategry. default 0 splits.performance
    #on entire training set data
    #forward and floating determines whether using SFS, SBS, SBFS, or SFFS
    #scoring options: accuracy, f1, precision, recall, roc_auc} for classifiers,
    #{'mean_absolute_error', 'mean_squared_error'/'neg_mean_squared_error',
    #'median_absolute_error', 'r2'
    #k_features how many features are selected on iteration
    #LinearRegression() type of model 
    #x: created in wrangle_for_select def
    #y: created by wrangle_for_select def
    #output:
    #figure: will show r2 for each k. Select k at elbow or where it plateaus
    #csv: dataframe of k column and r2 column 
    #feature_names_list: list of feature names for each k
    #: k column and r2 column
    #feature_answer: calculate determine which k change in curve is .1 or less 
    print('WARNING: This could take a long time to run. Consider using is_step if data have many features')
    if 'k_max' not in locals():
        k_max= (x.shape[1]+1)
    scores=[]
    feature_names_list=list()
    for k in range(1,k_max,is_step):
        sbfs = SFS(LinearRegression(),
                  k_features=k,
                  forward=True,
                  floating=False,
                  scoring = 'r2',
                  cv = 0)
        sbfs.fit(x, y)
        feature_names_list.append(sbfs.k_feature_names_)
        scores.append([sbfs.k_score_,k])
        del sbfs
    scores=pd.DataFrame(scores, columns=['x','y'])
    fig = plt.figure()
    plt.plot(scores['y'], scores['x'])
    fig.savefig('prefeature_selection_forward_feature_selection_results_MR_'+ str(time.time()) + '.png')
    plt.show()
    pd.concat([pd.DataFrame(feature_names_list),scores],axis=1).to_csv('prefeature_selection_forward_feature_selection_results_MR_'+ str(time.time()) + '.csv')
    possible=pd.DataFrame(np.where(scores['x'].diff()<.01))
    feature_answer=int(pd.DataFrame(np.where(((scores['x'].diff()<=k_change)==False))).T.max()+1)
    feature_list=pd.DataFrame(feature_names_list[feature_answer],columns=['features'])
    return feature_names_list, scores,feature_answer,feature_list


def foward_backward_selection(x,y,is_step,k_change,k_max):
    #Forward/Backward selection. Bidrectional method. 
    #Add feature one at a time and select feature wtih the minimumj p-value.  
    #iteratively create model with added features looking for the next best 
    #feature.Features added until none have minimum p-value. When a new feature
    #is added it checks other features and makes sure they still remain
    #significant. If they do not that feature is removed. 
    #Input
    #k_max: one way to reduce time of identifying k. Choose maximum number of 
    #features to consider
    #cv- determines the cross validation splitting strategry. default 0 splits.performance
    #on entire training set data
    #k_change: calculating the difference in r2 between each consecutive k and setting a threshold
    #for that difference. Default to .01 until further testing is performed. 
    #forward and floating determines whether using SFS, SBS, SBFS, or SFFS
    #scoring options: accuracy, f1, precision, recall, roc_auc} for classifiers,
    #{'mean_absolute_error', 'mean_squared_error'/'neg_mean_squared_error',
    #'median_absolute_error', 'r2'
    #k_features how many features are selected on iteration
    #LinearRegression() type of model 
    #x: created in wrangle_for_select def
    #y: created by wrangle_for_select def
    #figure: will show r2 for each k. Select k at elbow or where it plateaus
    #csv: dataframe of k column and r2 column 
    #feature_names_list: list of feature names for each k
    #: k column and r2 column
    #feature_answer: calculate determine which k change in curve is .1 or less 
    print('WARNING: This could take a long time to run. Consider using is_step if data have many features')

    if 'k_max' not in locals():
        k_max= (x.shape[1]+1)
    scores=[]
    feature_names_list=list()
    for k in range(1,k_max,is_step):
        print(k)
        sffs = SFS(LinearRegression(),
                  k_features=k,
                  forward=True,
                  floating=True,
                  cv = 0)
        sffs.fit(x, y)
        feature_names_list.append(sffs.k_feature_names_)
        scores.append([sffs.k_score_,k])
        del sffs

    scores=pd.DataFrame(scores, columns=['x','y'])
    fig = plt.figure()
    plt.plot(scores['y'], scores['x'])
    fig.savefig('prefeature_selection_fb_feature_selection_results_MR_'+ str(time.time()) + '.png')
    plt.show()
    pd.concat([pd.DataFrame(feature_names_list),scores],axis=1).to_csv('prefeature_selection_fb_feature_selection_results_MR_'+ str(time.time()) + '.csv')
    possible=pd.DataFrame(np.where(scores['x'].diff()<k_change))
    #find k where on a plot of score vs k, the difference between each consecutive k is .01 or less (default k_thresh). once this index is identified
    # go one step up. 
    feature_answer=int(pd.DataFrame(np.where(((scores['x'].diff()<=k_change)==False))).T.max()+1)
    feature_list=pd.DataFrame(feature_names_list[feature_answer],columns=['features'])   
    return feature_names_list,scores,feature_answer, feature_list


def backward_selection_method(x,y,is_step,k_change,k_max):
    #Backward elimination
    #Start with the full model and then remove features that do not reach significance/ highest 
    #p-value Continues until all features are significant
    #Input
    #k_max: one way to reduce time of identifying k. Choose maximum number of 
    #features to consider
    #cv- determines the cross validation splitting strategry. default 0 splits.performance
    #on entire training set data
    #forward and floating determines whether using SFS, SBS, SBFS, or SFFS
    #scoring options: accuracy, f1, precision, recall, roc_auc} for classifiers,
    #{'mean_absolute_error', 'mean_squared_error'/'neg_mean_squared_error',
    #'median_absolute_error', 'r2'
    #k_features how many features are selected on iteration
    #LinearRegression() type of model 
    #x: created in wrangle_for_select def
    #y: created by wrangle_for_select def
    #output:
    #figure: will show r2 for each k. Select k at elbow or where it plateaus
    #csv: dataframe of k column and r2 column 
    #feature_names_list: list of feature names for each k
    #: k column and r2 column
    #feature_answer: calculate determine which k change in curve is .1 or less 
    print('WARNING: This could take a long time to run. Consider using is_step if data have many features')
    if 'k_max' not in locals():
        k_max= (x.shape[1]+1)
    scores=[]
    feature_names_list=list()
    for k in range(1,k_max,is_step):
        print(k)
        sbfs = SFS(LinearRegression(),
                  k_features=k,
                  forward=False,
                  floating=False,
                  scoring = 'r2',
                  cv = 0)
        sbfs.fit(x, y)
        feature_names_list.append(sbfs.k_feature_names_)
        scores.append([sbfs.k_score_,k])
        del sbfs
    scores=pd.DataFrame(scores, columns=['x','y'])
    fig = plt.figure()
    plt.plot(scores['y'], scores['x'])
    fig.savefig('prefeature_selection_backward_feature_selection_results_MR_'+ str(time.time()) + '.png')
    plt.show()
    pd.concat([pd.DataFrame(feature_names_list),scores],axis=1).to_csv('prefeature_selection_backward_feature_selection_results_MR_'+ str(time.time()) + '.csv')
    possible=pd.DataFrame(np.where(scores['x'].diff()<.01))
    feature_answer=int(pd.DataFrame(np.where(((scores['x'].diff()<=k_change)==False))).T.max()+1)
    feature_list=pd.DataFrame(feature_names_list[feature_answer],columns=['features'])
    return feature_names_list,scores,feature_answer,feature_list

def check_selection(feature_names_list, feature_answer):
    #this function will show what the model features will be based on the k chosen from the selection method
    #and will contrast this list to a list of all features found in earlier Ks. Features in earlier Ks often lead to 
    #increasingly large changes in r2 and may be important. This function will allow visibility of all features in earlier Ks 
    #and identify those that may not be included in the chosen K. add_features function allows additional features to 
    #be added to feature list if desired.
    #INPUT
    #feature_names_list- produced by feature selection functions. List of features for each k
    #feature_answer- which k was chosen to identify features for final model
    #OUTPUT
    #
    print('Features in final model based on Feature selection')
    print(pd.DataFrame(feature_names_list[feature_answer],columns=['Final Model Features']))
    feature_popularity=pd.DataFrame(feature_names_list).head(feature_answer).stack().reset_index()[0].value_counts()
    print('Below is a dataframe of features and their frequencies for features K and earlier, where K represents features chosen for' + 
          'the final model and earlier Ks are features that created the most change (r2) in the model. Are there any common features to be added? Use' +
          'function add_features() to add')
    print(feature_popularity)
    
def add_features(add_features,feature_answer,feature_names_list):
    #add dataframe of features (column names) to be added to the model
    #INPUT
    #add_features example:
        # add_features=pd.DataFrame(['Name','Name'])
    #OUTPUT
    #feature_names_list: new feature list to included added
    feature_list=pd.concat([pd.DataFrame(feature_names_list[feature_answer]),add_features]).reset_index(drop=True)
    return feature_list
    
def create_dataset_from_fs(feature_list,x_train_pro,x_test_pro,index,predictor_name):
    #this function will create a new dataset based on results to selection process
    #Input
    #feature_names_list: list of features for each k from selection function
    #index: the k that is chosen or number of features based on selection results
    #x_train_pro: preprocesed data IV/features
    #y_train_pro: preprocessed data DV/predictor
    #Output
    # x_train_pro: train dataframe/ IV/Features reduce features list to those selected by feature selection
    #x_test_pro: test dataframe/ IV/Features reduce feature list to those selected by feature selection
    feature_list=pd.DataFrame(feature_list[index],columns=['features'])
    feature_list.columns=['features']
    x_train_pro_fs=x_train_pro[feature_list['features']]
    x_test_pro_fs=x_test_pro[feature_list['features']]
    return x_train_pro_fs,x_test_pro

def impute_mean(data):
    #function will go through a dataset's columns and compute mean for all nans
    #Input
    #data: dataset with nulls to be changed to mean of column
    #Output
    #dataset with nulls removed and mean added
    include=data[data.columns[data.dtypes.astype(str).isin(['int64','float64'])]]
    for i in range(0,(include.shape[1])):
        column_name=include.columns[i]
        data[column_name]=data[column_name].replace(np.nan, data[column_name].mean())
    return data

def impute_median(data):
    #function will go through a dataset's columns and compute mean for all nans
    #Input
    #data: dataset with nulls to be changed to mean of column
    #Output
    #dataset with nulls removed and mean added
    include=data[data.columns[data.dtypes.astype(str).isin(['int64','float64'])]]
    for i in range(0,(include.shape[1])):
        column_name=include.columns[i]
        data[column_name]=data[column_name].replace(np.nan, data[column_name].median())
    return data


def impute_mode(data):
    #function will go through a dataset's columns and compute mean for all nans
    #Input
    #data: dataset with nulls to be changed to mean of column
    #Output
    #dataset with nulls removed and mean added
    include=data[data.columns[data.dtypes.astype(str).isin(['object','category'])]]
    for i in range(0,(include.shape[1])):
        column_name=include.columns[i]
        data[column_name]=data[column_name].fillna(data[column_name].mode()[0])
    return data
 
def run_MR_model(x_data,x_test, y_test,y_data):
    #run linear regression model using preprocessed dataset
    #input
    #x_data: x_train_pro preprocesed iv/features dataframe
    #x_test: y_train_pro preprocessed dv/predictor dataframe
    #y_test: y_test test dataset dv/predictor dataframe
    #y_data: y train predictor/dv dataframe
    #output
    #y_test: predictor/dv for test samplerecode_to_binary
    #x_test: feature/dv for test sample
    #y_prediction: predictor/dv predicted values
    #residuals: y-test-y_prediction 
    #LR: LR model
    #Error includes the r2, mean squared error, and squared mean squared error
    LR = LinearRegression()
    LR.fit(x_data,y_data)
    y_prediction =  LR.predict(x_test)
    ytrain_prediction =  LR.predict(x_data)
    score=r2_score(y_test,y_prediction)
    train_score=r2_score(y_data,ytrain_prediction)
    print('Train and Test Model Performance')
    print('r2 socre for train and test ',[train_score, score])
    print('mean_sqrd_error is==',[mean_squared_error(y_data,ytrain_prediction),mean_squared_error(y_test,y_prediction)])
    print('root_mean_squared error of is==',[np.sqrt(mean_squared_error(y_data,ytrain_prediction)),np.sqrt(mean_squared_error(y_test,y_prediction))])
    Error_test=pd.DataFrame([score, mean_squared_error(y_test,y_prediction), np.sqrt(mean_squared_error(y_test,y_prediction))]).T
    Error_train=pd.DataFrame([score, mean_squared_error(y_data,ytrain_prediction), np.sqrt(mean_squared_error(y_data,ytrain_prediction))]).T
    residuals=y_test-y_prediction
    return y_test, x_test, residuals, y_prediction, LR, Error_train, Error_test

def plot_residuals(y_prediction, residuals, y_test): 
    # plot residuals with results from run_MR_model
    #Input
    #y_prediction- prediction produced by model
    #residuals- the difference between actual-predicted
    #y_test actuals for dv/predictor test sample
    #Output
    #two plots one with predicted as x and residuals as y and another 
    #actuals by x and residuals for y
    df=pd.concat([pd.DataFrame(y_prediction).reset_index(drop=True),residuals.reset_index(drop=True),y_test.reset_index(drop=True)],axis=1)
    df.columns=(['predicted','residuals','actuals'])
    fig, (ax1,ax2) = plt.subplots(1,2, figsize=(16,6))
    fig.suptitle('Residual Analysis')
    ax1.set_title('Residual vs Predicted')
    ax2.set_title('Residual vs Actuals')
    sns.scatterplot(data=df, x="predicted", y="residuals",ax=ax1)
    sns.scatterplot(data=df, x="actuals", y="residuals",ax=ax2)
    fig.savefig('ResidualAnalysis'+ str(time.time()) + '.png')

    
def scatter_corr(trainx, ypred):  
    #this function will use seaborn to make a scattter plot for each feature pair
    #For continuous predictors.   
    #Input
    #trainx: training data used in model IVs/features
    #ypred: training data used in model predictor/dv
    #Output
    #scatterplot saved to current directory
    final_set=pd.concat([trainx, ypred],axis=1)    
    g = sns.pairplot(final_set[final_set.columns],palette='tab20')
    g.savefig('features_scatterplt'+ str(time.time()) + '.png')
    
def try_transform(xtrain, ytrain, predictor_name,method_type):
    #function will apply one of two transforms to entire dataframe so data must
    #numeric. To be run on preprocessed data meant for model. Can be an early
    #step in preprocessing or done after baseline model created without transformations
    #should be done before feature selection
    #Input
    #xtrain: x_train_pro or x_train iv/features depending on at what point want transformation done
    #ytrain: y_train_pro or y_test predictor/dv dataframe
    #predictor_name: column name of predictor/dv as string
    #method_type: method 1 is log transform, method other than 1 is square root
    #Output:
    #transform_matrix: IVs and DV all transformed using specified method
    IVDV=pd.concat([pd.DataFrame(xtrain).reset_index(drop=True), pd.DataFrame(ytrain).reset_index(drop=True)],axis=1)
    if method_type==1:
        transform_matrix=pd.DataFrame((IVDV+100).apply(lambda x: np.log(x),axis=1).round(4))
    else:
        transform_matrix=pd.DataFrame(IVDV.apply(lambda x: np.sqrt(x),axis=1).round(2))
    return transform_matrix


 


